<?php
// Method I made for AQM


include_once('../_lib/libraries/grp/PHPExcel/Classes/PHPExcel.php');
$sql = "select 
`sp_tabulation_lvl_3`.`location` AS `location`,
`sp_tabulation_lvl_3`.`region` AS `region`,
sum(`sp_tabulation_lvl_3`.`2014-01`) as `2014_01`,
sum(`sp_tabulation_lvl_3`.`2014-02`) as `2014_02`,
sum(`sp_tabulation_lvl_3`.`2014-03`) as `2014_03`,
sum(`sp_tabulation_lvl_3`.`2014-04`) as `2014_04`,
sum(`sp_tabulation_lvl_3`.`2014-05`) as `2014_05`,
sum(`sp_tabulation_lvl_3`.`2014-06`) as `2014_06`,
sum(`sp_tabulation_lvl_3`.`2014-07`) as `2014_07`,
sum(`sp_tabulation_lvl_3`.`2014-08`) as `2014_08`,
sum(`sp_tabulation_lvl_3`.`2014-09`) as `2014_09`,
sum(`sp_tabulation_lvl_3`.`2014-10`) as `2014_10`,
sum(`sp_tabulation_lvl_3`.`2014-11`) as `2014_11`,
sum(`sp_tabulation_lvl_3`.`2014-12`) as `2014_12`,
sum(`sp_tabulation_lvl_3`.`2015-01`) as `2015_01`,
sum(`sp_tabulation_lvl_3`.`2015-02`) as `2015_02`,
sum(`sp_tabulation_lvl_3`.`2015-03`) as `2015_03`,
sum(`sp_tabulation_lvl_3`.`2015-04`) as `2015_04`,
sum(`sp_tabulation_lvl_3`.`2015-05`) as `2015_05`,
sum(`sp_tabulation_lvl_3`.`2015-06`) as `2015_06`,
sum(`sp_tabulation_lvl_3`.`2015-07`) as `2015_07`,
sum(`sp_tabulation_lvl_3`.`2015-08`) as `2015_08`,
sum(`sp_tabulation_lvl_3`.`2015-09`) as `2015_09`,
sum(`sp_tabulation_lvl_3`.`2015-10`) as `2015_10`,
sum(`sp_tabulation_lvl_3`.`2015-11`) as `2015_11`,
sum(`sp_tabulation_lvl_3`.`2015-12`) as `2015_12`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-01`)*10) as `3 Point-2015_01`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-02`)*10) as `3 Point-2015_02`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-03`)*10) as `3 Point-2015_03`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-04`)*10) as `3 Point-2015_04`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-05`)*10) as `3 Point-2015_05`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-06`)*10) as `3 Point-2015_06`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-07`)*10) as `3 Point-2015_07`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-08`)*10) as `3 Point-2015_08`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-09`)*10) as `3 Point-2015_09`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-10`)*10) as `3 Point-2015_10`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-11`)*10) as `3 Point-2015_11`,
round(sum(`sp_tabulation_lvl_3`.`3 Point-2015-12`)*10) as `3 Point-2015_12`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-01`) as `Redeemed-2015_01`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-02`) as `Redeemed-2015_02`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-03`) as `Redeemed-2015_03`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-04`) as `Redeemed-2015_04`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-05`) as `Redeemed-2015_05`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-06`) as `Redeemed-2015_06`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-07`) as `Redeemed-2015_07`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-08`) as `Redeemed-2015_08`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-09`) as `Redeemed-2015_09`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-10`) as `Redeemed-2015_10`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-11`) as `Redeemed-2015_11`,
sum(`sp_tabulation_lvl_3`.`Redeemed-2015-12`) as `Redeemed-2015_12`,
sum(`sp_tabulation_lvl_3`.`Total Redeemed-2015`) as `Total Redeemed_2015`,
sum(`sp_tabulation_lvl_3`.`RM Balance Redeemed-2015`) as `Balance Redeemed_2015`,
sum(`sp_tabulation_lvl_3`.`RM Balance Redeemed-2015`) as `RM Balance Redeemed_2015`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-01`) as `YTD Variant-2015_01`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-02`) as `YTD Variant-2015_02`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-03`) as `YTD Variant-2015_03`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-04`) as `YTD Variant-2015_04`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-05`) as `YTD Variant-2015_05`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-06`) as `YTD Variant-2015_06`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-07`) as `YTD Variant-2015_07`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-08`) as `YTD Variant-2015_08`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-09`) as `YTD Variant-2015_09`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-10`) as `YTD Variant-2015_10`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-11`) as `YTD Variant-2015_11`,
sum(`sp_tabulation_lvl_3`.`YTD Variant-2015-12`) as `YTD Variant-2015_12`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-01`) as `Outlet-2015_01`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-02`) as `Outlet-2015_02`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-03`) as `Outlet-2015_03`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-04`) as `Outlet-2015_04`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-05`) as `Outlet-2015_05`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-06`) as `Outlet-2015_06`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-07`) as `Outlet-2015_07`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-08`) as `Outlet-2015_08`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-09`) as `Outlet-2015_09`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-10`) as `Outlet-2015_10`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-11`) as `Outlet-2015_11`,
sum(`sp_tabulation_lvl_3`.`Outlet-2015-12`) as `Outlet-2015_12`,
sum(`sp_tabulation_lvl_3`.`Display-2015-01`) as `Display-2015_01`,
sum(`sp_tabulation_lvl_3`.`Display-2015-02`) as `Display-2015_02`,
sum(`sp_tabulation_lvl_3`.`Display-2015-03`) as `Display-2015_03`,
sum(`sp_tabulation_lvl_3`.`Display-2015-04`) as `Display-2015_04`,
sum(`sp_tabulation_lvl_3`.`Display-2015-05`) as `Display-2015_05`,
sum(`sp_tabulation_lvl_3`.`Display-2015-06`) as `Display-2015_06`,
sum(`sp_tabulation_lvl_3`.`Display-2015-07`) as `Display-2015_07`,
sum(`sp_tabulation_lvl_3`.`Display-2015-08`) as `Display-2015_08`,
sum(`sp_tabulation_lvl_3`.`Display-2015-09`) as `Display-2015_09`,
sum(`sp_tabulation_lvl_3`.`Display-2015-10`) as `Display-2015_10`,
sum(`sp_tabulation_lvl_3`.`Display-2015-11`) as `Display-2015_11`,
sum(`sp_tabulation_lvl_3`.`Display-2015-12`) as `Display-2015_12`,
sum(`sp_tabulation_lvl_3`.`Display-2015-12`) as `Display-2015_12_total`
from sp_tabulation_lvl_3
Where `sp_tabulation_lvl_3`.`status` = 1
group by 
`sp_tabulation_lvl_3`.`location`,
`sp_tabulation_lvl_3`.`region`";
 sc_select(dataset,$sql);

//create a file
$objPHPExcel = new PHPExcel();

$fileName = "Tabulation_Level_1_Jan.xlsx";

/*
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$fileName.'"');
header('Cache-Control: max-age=0');
*/

$objPHPExcel->setActiveSheetIndex(0);
$sheet = $objPHPExcel->getActiveSheet();

//merge cells header
$sheet->mergeCells('A1:A2');
$sheet->mergeCells('B1:B2');
$sheet->mergeCells('C1:C2');
$sheet->mergeCells('I1:J1');
$sheet->mergeCells('L1:N1');
$sheet->mergeCells('P1:T1');

//Auto size Column
//Auto size columns for each worksheet
for($col = 'A'; $col !== 'T'; $col++) {
    $sheet
        ->getColumnDimension($col)
        ->setAutoSize(true);
}

//borders config
$borders = array(
	'borders' => array(
			'allborders' => array(
		  		'style' => PHPExcel_Style_Border::BORDER_MEDIUM
			)
	)
);


//style config
$blue_header = array(
		'font' => array('bold' => true,),
		'alignment' => array(
			'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=>PHPExcel_Style_Alignment::VERTICAL_TOP
			),
		'fill'=> array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => '87CEFA')
		),
	);
$yellow_header = array(
		'font' => array('bold' => true,),
		'alignment' => array(
			'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			'vertical'=>PHPExcel_Style_Alignment::VERTICAL_TOP
			),
		'fill'=> array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'ffff00')
		),
		
	);
 
//apply borders
$sheet->getStyle('A1:' . 
    $sheet->getHighestColumn() . 
    $sheet->getHighestRow()
)->applyFromArray($borders);
 
 //apply style to header
//blue
$sheet->getStyle('A1')->applyFromArray($blue_header);
$sheet->getStyle('B1')->applyFromArray($blue_header);
$sheet->getStyle('C1')->applyFromArray($blue_header);
$sheet->getStyle('D1')->applyFromArray($blue_header);
$sheet->getStyle('E1')->applyFromArray($blue_header);
$sheet->getStyle('F1')->applyFromArray($blue_header);
$sheet->getStyle('G1')->applyFromArray($blue_header);
$sheet->getStyle('H1')->applyFromArray($blue_header);
$sheet->getStyle('I1')->applyFromArray($blue_header);
$sheet->getStyle('J1')->applyFromArray($blue_header);
$sheet->getStyle('K1')->applyFromArray($blue_header);
$sheet->getStyle('L1')->applyFromArray($blue_header);
$sheet->getStyle('M1')->applyFromArray($blue_header);
$sheet->getStyle('N1')->applyFromArray($blue_header);
$sheet->getStyle('O1')->applyFromArray($blue_header);
$sheet->getStyle('P1')->applyFromArray($blue_header);

//yellow
$sheet->getStyle('D2')->applyFromArray($yellow_header);
$sheet->getStyle('E2')->applyFromArray($yellow_header);
$sheet->getStyle('F2')->applyFromArray($yellow_header);
$sheet->getStyle('G2')->applyFromArray($yellow_header);
$sheet->getStyle('H2')->applyFromArray($yellow_header);
$sheet->getStyle('I2')->applyFromArray($yellow_header);
$sheet->getStyle('J2')->applyFromArray($yellow_header);
$sheet->getStyle('K2')->applyFromArray($yellow_header);
$sheet->getStyle('L2')->applyFromArray($yellow_header);
$sheet->getStyle('M2')->applyFromArray($yellow_header);
$sheet->getStyle('N2')->applyFromArray($yellow_header);
$sheet->getStyle('O2')->applyFromArray($yellow_header);
$sheet->getStyle('P2')->applyFromArray($yellow_header);
$sheet->getStyle('Q2')->applyFromArray($yellow_header);
$sheet->getStyle('R2')->applyFromArray($yellow_header);
$sheet->getStyle('S2')->applyFromArray($yellow_header);
$sheet->getStyle('T2')->applyFromArray($yellow_header);


//set cell value 
//Row 1
$sheet->setCellValue('A1', 'Location');
$sheet->setCellValue('B1', 'Region');
$sheet->setCellValue('C1', 'Distributors');
$sheet->setCellValue('D1', 'RM');
$sheet->setCellValue('E1', 'Point Earn');
$sheet->setCellValue('F1', 'Cumulative');
$sheet->setCellValue('G1', 'Point Redeem (Points)');
$sheet->setCellValue('H1', 'Point Redeem (Points)');
$sheet->setCellValue('H1', 'Point Redeem (RM)');
$sheet->setCellValue('I1', 'Point Balance');
$sheet->setCellValue('K1', 'RM Balance');
$sheet->setCellValue('L1', 'YTD Jan - Jan 2014 vs 2015');
$sheet->setCellValue('O1', 'No. of Outlet');
$sheet->setCellValue('P1', 'Display');

//Row 2
$sheet->setCellValue('D2', 'Jan-15');
$sheet->setCellValue('E2', 'Jan Jan-2015');
$sheet->setCellValue('F2', '15-Jan');
$sheet->setCellValue('G2', 'YTD Jan 2015');
$sheet->setCellValue('H2', 'YTD Jan 2015');
$sheet->setCellValue('I2', 'YTD Jan 2015');
$sheet->setCellValue('J2', 'YTD Jan 2015');
$sheet->setCellValue('K2', 'YTD Jan 2015');
$sheet->setCellValue('L2', 'YTD Jan 2015');
$sheet->setCellValue('M2', 'Jan-14');
$sheet->setCellValue('N2', 'YTD Jan-2014');
$sheet->setCellValue('O2', 'YTD Jan-2015');
$sheet->setCellValue('P2', 'Jan-15');
$sheet->setCellValue('Q2', 'Jan-14');
$sheet->setCellValue('R2', 'YTD Jan-2015');
$sheet->setCellValue('S2', 'Jan-15');
$sheet->setCellValue('T2', 'YTD Jan-2015');

$row = 3;
    foreach($dataset as $datasets)
	{
		$col = 0;
		$data = array(
			$datasets['location'],
			$datasets['region'],
			$datasets['2015_01'],
			$datasets['2015_01'],
			$datasets['3 Point-2015_01'],
			$datasets['3 Point-2015_01'],
			$datasets['Total Redeemed_2015'],
		    $datasets['RM Balance Redeemed_2015'],
		    $datasets['3 Point-2015_01']-$datasets['Redeemed-2015_01'],
			($datasets['3 Point-2015_01']-$datasets['Redeemed-2015_01'])*0.01,
			$datasets['2014_01'],
			$datasets['2015_01'],
			round((($datasets['2015_01']/$datasets['2014_01'])-1)*100,1,PHP_ROUND_HALF_DOWN),
			$datasets['2014_01'],
			$datasets['2015_01'],
			$datasets['Outlet-2015_01'],
			$datasets['Outlet-2015_01'],	
			$datasets['Display-2015_01'],
			$datasets['Display-2015_01']
		);

		foreach ($data as $d) {
		//insert data to excel
			$sheet->setCellValueByColumnAndRow($col++, $row, $d);
		}

		$row++;


		
		
	} 

// Rename sheet
$sheet->setTitle('Tabulation_Level_1_Jan');

// Save Excel file
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

$current_date = date('Y-m-d');
$fileName = 'Tabulation_Level_1_'.$current_date.'.xlsx';
ob_end_clean();
$objWriter->save($fileName);
rename($fileName, '../../output_file/'.$fileName);
?>

<a href="/aqloyalty/nestle/output_file/<?php echo $fileName;?>">Click to Download</a>

<?php

exit;


