<?php 
// down vote
// accepted
// It's a pass by reference. The variable inside the function "points" to the same data as the variable from the calling context.

function foo(&$test)
{
  $bar = 1;
}

$x = 0;
foo($x);
echo $x; // 1
?>