<!DOCTYPE html>
<html>
<head>
	<title>	AJAX UPLOAD</title>
</head>
<body>
	<input id="sortpicture" type="file" name="sortpic" />
	<button id="upload">Upload</button>

	<script type="text/javascript">
		$(document).ready(function(){
			$("#upload").on("click", function() {
				var file_data = $("#sortpicture").prop("files")[0];   
				var form_data = new FormData();                  
				form_data.append("file", file_data)
				alert(form_data);                             
				$.ajax({
					url: "/uploads",
					dataType: 'script',
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,                         
					type: 'post',
					success: function(){
						alert("works"); 
					}
				});
			});
		});
	</script>
</body>
</html>