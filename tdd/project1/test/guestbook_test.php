<?php 
require_once(dirname(__FILE__) . '../../../../simpletest/autorun.php');
require_once('../classes/guestbook.php');

class TestGuestbook extends UnitTestCase {
	function testViewGuestbookWithEntries()
	{
		$guestbook = new Guestbook();
        // Add new records first
		$guestbook->add("Bob", "Hi, I'm Bob.");
		$guestbook->add("Tom", "Hi, I'm Tom.");
		$entries = $guestbook->viewAll();

		$count_is_greater_than_zero = (count($entries) > 0);

		//assertTrue($x) : Fail if $x is false
		$this->assertTrue($count_is_greater_than_zero);

		//assertIsA($x, $t) : Fail if $x is not the class or type $t
		$this->assertIsA($entries, 'array');


		foreach($entries as $entry) {
			$this->assertIsA($entry, 'array');
			$this->assertTrue(isset($entry['name']));
			$this->assertTrue(isset($entry['message']));
		}
	}

	function testViewGuestbookWithNoEntries()
	{
		$guestbook = new Guestbook();
        $guestbook->deleteAll(); // Delete all the entries first so we know it's an empty table
        $entries = $guestbook->viewAll();

        //assertEqual($x, $y) : Fail if $x == $y is false
        $this->assertEqual($entries, array());
    }
}