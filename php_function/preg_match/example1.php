<?php 
// Syntax
// int preg_match (string pattern, string string [, array pattern_array], [, int $flags [, int $offset]]]);
// Definition and Usage
// The preg_match() function searches string for pattern, returning true if pattern exists, and false otherwise.

// If the optional input parameter pattern_array is provided, then pattern_array will contain various sections of the subpatterns contained in the search pattern, if applicable.

// If this flag is passed as PREG_OFFSET_CAPTURE, for every occurring match the appendant string offset will also be returned

// Normally, the search starts from the beginning of the subject string. The optional parameter offset can be used to specify the alternate place from which to start the search.

// Return Value
// Returns true if pattern exists, and false otherwise.
// Example
// Following is the piece of code, copy and paste this code into a file and verify the result.
 $line = "Vi is the greatest word processor ever created!";
   // perform a case-Insensitive search for the word "Vi"
   
   if (preg_match("/\bVi\b/i", $line, $match)) :
      print "Match found!";
      endif;

?>