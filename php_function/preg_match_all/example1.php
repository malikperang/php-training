<?php 
// Syntax
// int preg_match_all (string pattern, string string, array pattern_array [, int order]);
// Definition and Usage
// The preg_match_all() function matches all occurrences of pattern in string.

// It will place these matches in the array pattern_array in the order you specify using the optional input parameter order. There are two possible types of order −

// PREG_PATTERN_ORDER − is the default if the optional order parameter is not included. PREG_PATTERN_ORDER specifies the order in the way that you might think most logical; $pattern_array[0] is an array of all complete pattern matches, $pattern_array[1] is an array of all strings matching the first parenthesized regexp, and so on.

// PREG_SET_ORDER − will order the array a bit differently than the default setting. $pattern_array[0] will contain elements matched by the first parenthesized regexp, $pattern_array[1] will contain elements matched by the second parenthesized regexp, and so on.

// Return Value
// Returns the number of matchings.
// Example
// Following is the piece of code, copy and paste this code into a file and verify the result.


$userinfo = "Name: <b>John Poul</b> <br> Title: <b>PHP Guru</b>";
preg_match_all ("/<b>(.*)<\/b>/U", $userinfo, $pat_array);

print $pat_array[0][0]." <br> ".$pat_array[0][1]."\n";
?>
