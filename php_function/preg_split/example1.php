<?php
// Syntax
// array preg_split (string pattern, string string [, int limit [, int flags]]);
// Definition and Usage
// The preg_split() function operates exactly like split(), except that regular expressions are accepted as input parameters for pattern.

// If the optional input parameter limit is specified, then only limit number of substrings are returned.

// flags can be any combination of the following flags −

// PREG_SPLIT_NO_EMPTY − If this flag is set, only non-empty pieces will be returned by preg_split().

// PREG_SPLIT_DELIM_CAPTURE − If this flag is set, parenthesized expression in the delimiter pattern will be captured and returned as well.

// PREG_SPLIT_OFFSET_CAPTURE − If this flag is set, for every occurring match the appendant string offset will also be returned.

// Return Value
// Returns an array of strings after splitting up a string.
// Example
// Following is the piece of code, copy and paste this code into a file and verify the result.


$ip = "123.456.789.000"; // some IP address
$iparr = split ("/\./", $ip); 

print "$iparr[0] <br />";
print "$iparr[1] <br />" ;
print "$iparr[2] <br />"  ;
print "$iparr[3] <br />"  ;
?>
